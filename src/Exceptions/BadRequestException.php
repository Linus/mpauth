<?php
/**
 * Created by PhpStorm.
 * User: Linus
 * Date: 2019/1/21
 * Time: 17:56
 */

namespace Linus\Laravel\Exceptions;


use Throwable;

class BadRequestException extends ApiException
{
    public function __construct(string $message = "", int $code = ExceptionCode::BAD_REQUEST, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}