<?php

namespace Linus\Laravel\Exceptions;

use Throwable;

class TokenNotFoundException extends ApiException
{
    //
    public function __construct(string $message = "", int $code = ExceptionCode::TOKEN_NOT_FOUND, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
