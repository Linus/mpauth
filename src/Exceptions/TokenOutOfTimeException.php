<?php

namespace Linus\Laravel\Exceptions;

use Throwable;

class TokenOutOfTimeException extends ApiException
{
    //
    public function __construct(string $message = "", int $code = ExceptionCode::TOKEN_OUT_OF_TIME, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
