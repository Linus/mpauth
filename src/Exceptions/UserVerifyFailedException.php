<?php
/**
 * Created by PhpStorm.
 * User: Linus
 * Date: 2019/1/16
 * Time: 8:35
 */

namespace Linus\Laravel\Exceptions;


use Throwable;

class UserVerifyFailedException extends ApiException
{
    public function __construct(string $message = "", int $code = ExceptionCode::USER_VERIFY_FAILED, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}