<?php

namespace Linus\Laravel\Exceptions;

use Throwable;

class SessionNotFoundException extends ApiException
{
    //
    public function __construct(string $message = "", int $code = ExceptionCode::SESSION_NOT_FOUND, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
