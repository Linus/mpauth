<?php
/**
 * Created by PhpStorm.
 * User: Linus
 * Date: 2019/1/15
 * Time: 16:40
 */

namespace Linus\Laravel\Exceptions;

use Exception;
use Throwable;

class ApiException extends Exception
{
public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
{
    if (empty($message)) {
        $message = config('errorcode.code')[$code];
    }
    parent::__construct($message, $code, $previous);
}
}