<?php
/**
 * Created by PhpStorm.
 * User: Linus
 * Date: 2019/1/15
 * Time: 18:08
 */

namespace Linus\Laravel\Exceptions;

use Throwable;

class UserNotFoundException extends ApiException
{
    public function __construct(string $message = "", int $code = ExceptionCode::USER_NOT_FOUND , Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}