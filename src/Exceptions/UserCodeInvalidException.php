<?php

namespace Linus\Laravel\Exceptions;

use Throwable;

class UserCodeInvalidException extends ApiException
{
    //
    public function __construct(string $message = "", int $code = ExceptionCode::USER_CODE_INVALID, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
