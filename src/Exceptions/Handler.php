<?php

namespace Linus\Laravel\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Linus\Laravel\Controllers\ApiController;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
//        \Illuminate\Auth\AuthenticationException::class,
//        \Illuminate\Auth\Access\AuthorizationException::class,
//        \Symfony\Component\HttpKernel\Exception\HttpException::class,
//        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
//        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // 如果config配置debug为true ==>debug模式的话让laravel自行处理
//        $debug_status = config('app.debug'); // .env文件配置
//        if ($debug_status) {
//            return parent::render($request, $exception);
//        }
        //返回API模式
        //如果是接口请求，则抛出json
        if ($request->is('api/*')) {
            // 只处理自定义的APIException异常
            if ($exception instanceof ApiException) {
                $debug_status = config('app.debug'); // .env文件配置
                //如果不是调试访问来源也不是微信小程序
                if (!starts_with($request->header('referer'), REFERER) && !$debug_status) {
                    return response()->json('bad request');
                }
                else {
                    //统一输出格式
                    return response()->json(ApiController::output(2, $exception->getCode(), []));
                }
            }

            # 继续写其他自定义异常
            /***
             * code
             *
             * ***/
        }
        return parent::render($request, $exception);
    }
}
