<?php

namespace Linus\Laravel\Exceptions;

use Throwable;
use Exception;

class TokenNotMatchException extends ApiException
{
    //
    public function __construct(string $message = "", int $code = ExceptionCode::TOKEN_NOT_MATCH, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
