<?php
/**
 * Created by PhpStorm.
 * User: Linus
 * Date: 2019/1/15
 * Time: 3:49
 */

namespace Linus\Laravel\Exceptions;


class ExceptionCode
{
    const BAD_REQUEST = 10001;

    const TOKEN_NOT_FOUND = 40001;
    const USER_CODE_INVALID = 40002;
    const TOKEN_OUT_OF_TIME = 40003;
    const ENCRYPT_DATA_INVALID = 40004;
    const USER_NOT_FOUND = 40005;
    const TOKEN_NOT_MATCH = 40006;
    const SESSION_NOT_FOUND = 40007;
    const USER_VERIFY_FAILED = 40008;
}