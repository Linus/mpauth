<?php

namespace Linus\Laravel\Controllers;

use GrahamCampbell\Throttle\Facades\Throttle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Linus\Laravel\Events\MiniProgramUserAuthorized;
use Linus\Laravel\Exceptions\EncryptDataInvalidException;
use Linus\Laravel\Exceptions\UserCodeInvalidException;
use Linus\Laravel\Exceptions\UserNotFoundException;
use Linus\Laravel\Models\User;
use Illuminate\Support\Facades\Validator;
use Linus\Laravel\Resources\Token;
use Linus\Laravel\Resources\User as UserResource;
use App\Http\Controllers\Controller;

class WeChatController extends ApiController
{
    public static function needUserInfo($updateTime)
    {
        //是否需要更新用户信息
        if (empty($updateTime)) {
            $getUserInfo = true;
        } else if (!empty($updateTime) && (time() - strtotime($updateTime) > 12 * 3600)) {
            $getUserInfo = true;
        } else {
            $getUserInfo = false;
        }
        return $getUserInfo;
    }

    public function wxLogin(Request $request, $isNewSession = false)
    {
        // 获取微信小程序app实例
        $miniProgram = app('mpauth.mini_program');
        //$isNewUser = false;
        $result = $miniProgram->auth->session($request->input('code'));
        $user = User::firstOrCreate(['mini_program' => $result['openid']]);
        //新用户第一次登陆
        if (isset($result['unionid'])) {
            $user->fill([
                'token' => User::createToken(),
                'session_key' => $result["session_key"],
                'mini_program' => $result['openid'],
                'union_id' => $request['unionid'],
                'expires_in' => 7200,
                'request_time' => time()
            ])->save();
        } else {
            $user->fill([
                'token' => User::createToken(),
                'session_key' => $result["session_key"],
                'mini_program' => $result['openid'],
                'expires_in' => 7200,
                'request_time' => time()
            ])->save();
        }

        //第一次登陆的话给用户创建session，提高token时间检查效率
        session()->put([
            'token' => $user['token'],
            'expires_in' => $user['expires_in'],
            'request_time' => $user['request_time']
        ]);
        //调用用户认证事件
        //Event::fire(new MiniProgramUserAuthorized($user, $isNewSession, $isNewUser));
        return $this->success([
            'token' => session()->get('token'),
            'session_id' => session()->getId(),
            'get_user_info' => self::needUserInfo($user->user_info_updated_at)
        ]);
    }

    public function getMiniProgramUserInfo(Request $request)
    {
        $data = json_decode($request->input('data'), true);
        if (Validator::make($data, [
            'raw_data' => 'required',
            'iv' => 'required',
            'signature' => 'required',
            'encrypted_data' => 'required'
        ])->fails()) {
            throw new EncryptDataInvalidException();
        } else {
            $miniProgram = app('mpauth.mini_program');
            $user = User::where('token', $request->header('token'))->first();
            if (empty($user)) {
                throw new UserNotFoundException();
            } else {
                $decryptedData = $miniProgram->encryptor->decryptData($user->session_key, $data['iv'], $data['encrypted_data']);
                $user->fill([
                    'user_info_updated_at' => now(),
                    'nick_name' => $decryptedData['nickName'],
                    'avatars' => $decryptedData['avatarUrl'],
                    'sex' => $decryptedData['gender'],
                    'mini_program' => $decryptedData['openId'],
                    'type' => 0,
                    'from' => 'miniprogram'
                ]);
                if (!empty($decryptedData['unionId'])) {
                    $user->fill([
                        'union_id' => $decryptedData['unionId']
                    ]);
                };
                $user->save();
                return $this->success(['user_id' => $user->id]);
            }
        }
    }

    public function login(Request $request)
    {
        if (!$request->header('token') || !session()->has('token')) {
            $isNewSession = true;
        } else {
            $isNewSession = false;
        }
        if (Validator::make($request->all(), ['code' => 'required|string:32'])->fails()) {
            //没有传回code，开发阶段可能是错误，上线以后大概率是测试或者非法调用
            throw new UserCodeInvalidException();
        } else {
            return $this->wxLogin($request, $isNewSession);
        }
    }
}