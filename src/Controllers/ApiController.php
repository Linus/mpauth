<?php

namespace Linus\Laravel\Controllers;

use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    //
    public function success($data = [])
    {
//        return response()->json(self::output(1, 200, $data));
        return json_encode(self::output(1, 200, $data));
    }

    //目前用抛出异常方式取代，除非约定好不要调用此函数
    public function fail($code, $data = [])
    {
        return response()->json(self::output(2, $code, $data));
    }

    public static function output($status, $code, $data)
    {
        return [
            'status' => $status,
            'code' => $code,
            'message' => config('errorcode.code')[$code],
            'data' => $data,
        ];
    }
}
