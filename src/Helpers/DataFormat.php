<?php
/**
 * Created by PhpStorm.
 * User: kai.qiu
 * Date: 17/3/20
 * Time: 下午9:32
 */

namespace Linus\Laravel\Helpers;

trait DataFormat
{

    public static function format($data)
    {
        if($data == null)
        {
            $data = [];
        }
        if(is_object($data))
        {
            $data =  $data instanceof Self?  (array) $data->attributes : (array) $data;
        }
        $data = isset(self::$formatKey) ? array_only($data, array_keys(self::$formatKey)):[];

        foreach($data as $key=>$value)
        {
            switch(self::$formatKey[$key])
            {
                case 'int':
                    $data[$key] = (int) $value;
                    break;

                case 'decimal':
                case 'float':
                case 'double':
                    $data[$key] = (double) $value;
                    break;

                case 'string':
                    $data[$key] = (string) $value;
                    break;

                case 'image_url':
                    $data[$key] = image_url($value);
                    break;

                case 'default':

                    break;

                case 'json_decode':
                    $data[$key] = json_decode($value, true);
                    break;

                default:;
            }
        }

        return $data;
    }

    public static function image_url($url)
    {
        if(empty($url))        {
            return $url;
        }
        $path = parse_url($url);

        if(empty($path['host']))
        {
            $url = $url[0] == '/'? config('filesystems.disks.image.url').$url:config('filesystems.disks.image.url').'/'.$url;
        }

        return $url;
    }
}