<?php

namespace Linus\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\Facades\Image;

class User extends Model
{
    use \Linus\Laravel\Helpers\DataFormat;
    protected $table = 'users';
    protected $dates = [''];

    protected $guarded = ['id', 'created_at'];

    public static $sexConf = [0=>'未知', 1=>'男', 2=>'女', ];

    public static  $formatKey = ['id'=>'int', 'nick_name'=>'string', 'avatars'=>'url', 'sex'=>'int', 'birthday'=>'string', 'phone'=>'string', 'real_name'=>'string', 'height'=>'float',  'weight'=>'float',  'real_weight'=>'float', 'email'=>'string', 'is_vip'=>'string', 'vip_end_time'=>'string', 'mac'=>'string', 'zero_coin'=>'int' ];

    public static function createToken()
    {
        return md5(microtime()).uniqid();
    }

    public function getAllInfo()
    {
        return User::format($this);
    }

    public function decreaseZeroCoin($poins)
    {
        include_once(app_path('libs/youzan/YZTokenClient.php'));

        $token = $this->getYouzanToken();

        $client = new \YZTokenClient($token);

        $method = 'youzan.crm.customer.points.decrease'; //要调用的api名称
        $api_version = '3.0.1'; //要调用的api版本号

        $my_params = [
            'points' => $poins,
            'mobile' => $this->phone,
        ];

        $my_files = [
        ];

        $rs = $client->post($method, $api_version, $my_params, $my_files);

        if($rs['response']['is_success'] == 'true')
        {
            return true;
        }

        return false;
    }

    public function increaseZeroCoin($poins)
    {
        include_once(app_path('libs/youzan/YZTokenClient.php'));

        $token = $this->getYouzanToken();

        $client = new \YZTokenClient($token);

        $method = 'youzan.crm.customer.points.increase'; //要调用的api名称
        $api_version = '3.0.1'; //要调用的api版本号

        $my_params = [
            'points' => $poins,
            'mobile' => $this->phone,
        ];

        $my_files = [
        ];

        $rs = $client->post($method, $api_version, $my_params, $my_files);

        if($rs['response']['is_success'] == 'true')
        {
            return true;
        }

        return false;
    }

    public function getZeroCoin()
    {
        include_once(app_path('libs/youzan/YZTokenClient.php'));

        $token = $this->getYouzanToken();

        $client = new \YZTokenClient($token);

        $method = 'youzan.crm.fans.points.get'; //要调用的api名称
        $api_version = '3.0.1'; //要调用的api版本号

        $my_params = [
            'mobile' => $this->phone,
        ];

        $my_files = [
        ];

        $rs = $client->post($method, $api_version, $my_params, $my_files);

        if(isset($rs['response']['point']))
        {
            $this->zero_coin = $rs['response']['point'];
            $this->save();

            return $rs['response']['point'];
        }

        return false;
    }

    public function getYouzanToken()
    {
        $token = Cache::get('youzan_access_token');

        if(!empty($token))
        {
            //return $token;
        }
        include_once(app_path('libs/youzan/YZGetTokenClient.php'));

        $id = config("app.youzan.client_id");
        $secret = config("app.youzan.client_secret");
        $shopId = config("app.youzan.shop_id");

        $youzan = new \YZGetTokenClient($id, $secret);

        $rs = $youzan->get_token('self', ['kdt_id'=>$shopId]);

        if(isset($rs['access_token']))
        {
            $token = $rs['access_token'];
            Cache::put('youzan_access_token', $token, $rs['expires_in']/2);
        }

        return $token;
    }

    public function addZeroCoin()
    {
        $nodes = HiitClock::where('user_id', $this->id)->where('date', date('Y-m-d'))->orderby('clock_time', 'asc')->get();

        $over = 0;
        foreach($nodes as $key=>$node)
        {
            if($key < 3 && $node->coin_num >= 20)
            {
                $over++;
            }
        }

        if(count($nodes) >= 3)
        {
            $node = ZeroDetail::where('user_id', $this->id)->where('created_at', 'like', date('Y-m-d').'%')->where('remark', '每日上传三次燃脂指数')->first();

            if(empty($node))
            {
                $node = new ZeroDetail();

                $node->fill(
                    [
                        'user_id'=>$this->id,
                        'type'=>1,
                        'zero_coin'=>3,
                        'remark'=>'每日上传三次燃脂指数',
                        'status'=>0,
                    ]
                );

                $node->save();
            }
        }

        if($over == 3)
        {
            $node = ZeroDetail::where('user_id', $this->id)->where('created_at', 'like', date('Y-m-d').'%')->where('remark', '三次燃脂指数超过20')->first();

            if(empty($node))
            {
                $node = new ZeroDetail();

                $node->fill(
                    [
                        'user_id'=>$this->id,
                        'type'=>1,
                        'zero_coin'=>5,
                        'remark'=>'三次燃脂指数超过20',
                        'status'=>0,
                    ]
                );

                $node->save();
            }
        }
    }

    public function getRoundAvatars($size)
    {
        $dir = config('filesystems.disks.image.root');

        $resizeDir = $dir.'/resize';


        if(!is_dir($resizeDir))
        {
            mkdir($resizeDir);
        }

        $toFile = $resizeDir.'/'.md5($this->avatars.$size).'.png';

        if(is_file($toFile))
        {
            return $toFile;
        }

        $im = Image::make($this->avatars)->resize($size, $size );

        $im->filter(new RadiusFilter( $size * 0.5 ));

        $im->save($toFile);

        return $toFile;

    }

    public function getAddr()
    {
        $data = [
            'province'=>$this->province,
            'city'=>$this->city,
            'county'=>$this->county,
            'addr'=>$this->rec_addr,
            'name'=>$this->rec_name,
            'phone'=>$this->rec_phone,
        ];

        return $data;
    }

}
