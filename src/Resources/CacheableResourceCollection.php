<?php
/**
 * Created by PhpStorm.
 * User: Linus
 * Date: 2019/1/11
 * Time: 15:55
 */

namespace Linus\Laravel\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Cache;

class CacheableResourceCollection extends ResourceCollection
{
    public $key;

    public function __construct($resource)
    {
        $this->key = $resource;
        if (!$this->check()){
            $model = \App::make($resource)->all();
            parent::__construct($model);
            Cache::put($this->key, $this->jsonSerialize(), 1);
        }
    }

    public function check()
    {
        if ($this->key) {
            return Cache::has($this->key);
        } else {
            return false;
        }
    }

    public function getCollection()
    {
        return Cache::get($this->key);
    }

    public function toResponse($request)
    {
        return Cache::get($this->key);
    }
}