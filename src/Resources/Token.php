<?php
/**
 * Created by PhpStorm.
 * User: Linus
 * Date: 2019/1/9
 * Time: 22:21
 */

namespace Linus\Laravel\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


class Token extends JsonResource
{
    public function toArray($request)
    {
        return [
            'token' => session()->get('token'),
            'session_id' => session()->getId()
        ];
    }
}
