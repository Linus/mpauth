<?php

/*
 * This file is part of the overtrue/laravel-wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Linus\Laravel\Events;

use Illuminate\Queue\SerializesModels;
use Linus\Laravel\Models\User;

class MiniProgramUserAuthorized
{
    use SerializesModels;

    protected $user;

    protected $isNewSession;

    protected $isNewUser;

    /**
     * Create a new event instance.
     *
     * @param \Linus\Laravel\Models\User $user
     * @param bool                     $isNewSession
     * @param bool                     $isNewUser
     */
    public function __construct(User $user, $isNewSession = false, $isNewUser = false)
    {
        $this->user = $user;
        $this->isNewSession = $isNewSession;
        $this->isNewUser = $isNewUser;
    }

    /**
     * Retrieve the authorized user.
     *
     * @return \Linus\Laravel\Models\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * The name of official account.
     *
     * @return string
     */
    public function getAccount()
    {
        //return $this->account;
    }

    /**
     * Check the user session is first created.
     *
     * @return bool
     */
    public function isNewSession()
    {
        return $this->isNewSession;
    }

    public function isNewUser()
    {
        return $this->isNewUser;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
