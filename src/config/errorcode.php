<?php
/**
 * Created by PhpStorm.
 * User: Linus
 * Date: 2019/1/20
 * Time: 18:59
 */
/*
    const TOKEN_NOT_FOUND = 40001;
    const USER_CODE_INVALID = 40002;
    const TOKEN_OUT_OF_TIME = 40003;
    const ENCRYPT_DATA_INVALID = 40004;
    const USER_NOT_FOUND = 40005;
    const TOKEN_NOT_MATCH = 40006;
    const SESSION_NOT_FOUND = 40007;
    const USER_VERIFY_FAILED = 40008;

    const ORDER_INFO_INCOMPLETE = 50001;
    const ORDER_UNIFY_FAILED = 50002;
    const ORDER_SAVE_FAILED = 50003;
    const ORDER_NOT_FOUND = 50004;
    const ORDER_STATUS_ERROR = 50005;
    const ORDER_PAY_NOT_ALLOWED = 50006;
    const ORDER_ALREADY_PAID = 50007;

    const GOODS_NOT_EXIST = 51001;
 */
return [
    'code' => [
        200 => '成功',

        10001 => '错误的请求（也可能是不明来源请求）',

        40001 => '用户令牌没有找到',
        40002 => '用户code没有找到或code不正确',
        40003 => '用户令牌过期',
        40004 => '用户加密数据不正确',
        40005 => '用户没有找到',
        40006 => '用户令牌不匹配',
        40007 => '用户服务器数据错误',
        40008 => '用户验证失败',

        //业务
        50001 => '订单信息不完整',
        50002 => '订单下单失败',
        50003 => '订单保存失败',
        50004 => '订单没有找到',
        50005 => '订单状态错误',
        50006 => '订单不允许支付',
        50007 => '订单已经支付',

        51001 => '货物没有找到',
    ],
];