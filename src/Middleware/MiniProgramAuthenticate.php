<?php

/*
 * This file is part of the overtrue/laravel-wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Linus\Laravel\Middleware;

use Closure;
use Linus\Laravel\Controllers\ApiController;
use Linus\Laravel\Controllers\WeChatController;
use Linus\Laravel\Exceptions\BadRequestException;
use Linus\Laravel\Exceptions\SessionNotFoundException;
use Linus\Laravel\Exceptions\TokenNotFoundException;
use Linus\Laravel\Exceptions\TokenNotMatchException;
use Linus\Laravel\Exceptions\TokenOutOfTimeException;
use Linus\Laravel\Models\User;

/**
 * Class MiniProgramAuthenticate.
 */
class MiniProgramAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     * @throws \Linus\Laravel\Exceptions\TokenNotMatchException
     * @throws \Linus\Laravel\Exceptions\TokenOutOfTimeException
     * @throws \Linus\Laravel\Exceptions\TokenNotFoundException
     * @throws \Linus\Laravel\Exceptions\SessionNotFoundException
     */
    public function handle($request, Closure $next)
    {
        if (starts_with($request->header('referer'), REFERER)) {
            if (starts_with(strtolower($request->getRequestUri()), '/api/init')) {
                return $next($request);
            }
            //如果调用登录，直接放行
            if (starts_with(strtolower($request->getRequestUri()), '/api/login')) {
                return $next($request);
            }
            if ($request->header('token') && session()->has('token')) {
                if ($request->header('token') === session()->get('token')) {
                    //verify visitor's token, if token already out time, abandon it. visitor must re-login refresh token.
                    if (time() > (session()->get('request_time') + session()->get('expires_in'))) {
                        // 如果登录已经超时，需要重新登录，更新User表的token使用户无法登录
                        throw new TokenOutOfTimeException();
                    } else {
                        if (starts_with(strtolower($request->getRequestUri()), '/api/ping')) {
                            return response()->json(ApiController::output(1, 200, ['pong' => 'ping', 'get_user_info' => false]));
                        } else {
                            return $next($request);
                        }
                    }
                } else {
                    throw new TokenNotMatchException();
                }
            } else {
                if (!session()->has('token')) {
                    throw new SessionNotFoundException();
                } else {
                    throw new TokenNotFoundException();
                }
            }
        } else {
            throw new BadRequestException();
        }
    }
}
