<?php
/**
 * Created by PhpStorm.
 * User: Linus
 * Date: 2019/1/7
 * Time: 17:23
 */

namespace Linus\Laravel;

use Illuminate\Support\Facades\Facade as LaravelFacade;

class Facade extends laravelFacade
{
    protected static function getFacadeAccessor()
    {
        return 'mpauth.mini_program';
    }

    public static function officialAccount($name = '')
    {
        return $name ? app('mpauth.official_account.'.$name) : app('mpauth.official_account');
    }

    /**
     * @return \EasyWeChat\Payment\Application
     */
    public static function payment($name = '')
    {
        return $name ? app('mpauth.payment.'.$name) : app('mpauth.payment');
    }

    /**
     * @return \EasyWeChat\MiniProgram\Application
     */
    public static function miniProgram($name = '')
    {
        return $name ? app('mpauth.mini_program.'.$name) : app('mpauth.mini_program');
    }

}