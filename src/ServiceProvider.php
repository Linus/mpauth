<?php

namespace Linus\Laravel;

use EasyWeChat\MiniProgram\Application as MiniProgram;
use EasyWeChat\OfficialAccount\Application as OfficialAccount;
use EasyWeChat\Payment\Application as Payment;
use Illuminate\Foundation\Application as LaravelApplication;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

define('REFERER', 'https://servicewechat.com/' . config('wechat.mini_program.default')['app_id']);


class MiniProgramAuthServiceProvider extends LaravelServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }

    public function setConfig()
    {
        $source = realpath(__DIR__ . '/config/wechat.php');
        $config = realpath(__DIR__ . '/config/config.php');
        $errorcode = realpath(__DIR__ . '/config/errorcode.php');

        if ($this->app instanceof LaravelApplication && $this->app->runningInConsole()) {
            if (!file_exists(config_path('wechat.php'))) {
                $this->publishes([$source => config_path('wechat.php')]);
            }
            if (!file_exists(config_path('errorcode.php'))) {
                $this->publishes([$errorcode => config_path('errorcode.php')]);
            }
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->setConfig();

        $apps = [
            'mini_program' => MiniProgram::class,
            'official_account' => OfficialAccount::class,
            'payment' => Payment::class,
        ];

        foreach ($apps as $name => $class) {
            if (empty(config('wechat.' . $name))) {
                continue;
            }

            $accounts = config('wechat.' . $name);

            foreach ($accounts as $account => $config) {
                $this->app->singleton("mpauth.{$name}.{$account}", function ($laravelApp) use ($name, $account, $config, $class) {
                    $app = new $class(array_merge(config('wechat.defaults', []), $config));
                    if (config('wechat.defaults.use_laravel_cache')) {
                        $app['cache'] = new CacheBridge($laravelApp['cache.store']);
                    }
                    $app['request'] = $laravelApp['request'];

                    return $app;
                });
            }
            $this->app->alias("mpauth.{$name}.default", 'mpauth.' . $name);
            $this->app->alias('mpauth.' . $name, $class);
        }
    }
}
